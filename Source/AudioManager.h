/*
  ==============================================================================

    AudioDeviceManager.h
    Created: 22 Apr 2019 10:26:31pm
    Author:  gllm

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

//==============================================================================
class AudioManager :		public AudioAppComponent,
							public ChangeListener,
							private Timer
{
public:
	//==============================================================================
	AudioManager();
	~AudioManager();

	//==============================================================================
	void prepareToPlay(int, double) override;
	void getNextAudioBlock(const AudioSourceChannelInfo& bufferToFill) override;
	void releaseResources() override;

	//==============================================================================
	void paint(Graphics& g) override;
	void resized() override;

private:
	//==============================================================================
	void changeListenerCallback(ChangeBroadcaster*);

	static String getListOfActiveBits(const BigInteger& b);

	void timerCallback() override;

	void dumpDeviceInfo();
	void logMessage(const String& m);

	//==========================================================================
	Random random;
	AudioDeviceSelectorComponent audioSetupComp;
	Label cpuUsageLabel;
	Label cpuUsageText;
	TextEditor diagnosticsBox;

	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(AudioManager)
};

//==============================================================================
class AudioManagerContainer : public DocumentWindow
{
public:
	//==============================================================================
	AudioManagerContainer(const String& name, Colour backgroundColour, int buttonsNeeded)
		: DocumentWindow(name, backgroundColour, buttonsNeeded)
	{
		setContentOwned(&manager, false);
	}

	~AudioManagerContainer() {};

	void closeButtonPressed()
	{
		delete this;
	}

private:
	AudioManager manager;

	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(AudioManagerContainer)
};