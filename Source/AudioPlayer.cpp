/*
  ==============================================================================

    AudioPlayer.cpp
    Created: 21 Apr 2019 4:21:12pm
    Author:  gllm

  ==============================================================================
*/

#include "../JuceLibraryCode/JuceHeader.h"
#include "AudioPlayer.h"

//==============================================================================
AudioPlayer::AudioPlayer(AudioBufferProcessor &bufProc) :	state(Stopped),
								thumbnailCache(5),
								thumbnailComp(512, formatManager, thumbnailCache),
								positionOverlay(transportSource)
{
    // In your constructor, you should add any child components, and
    // initialise any special settings that your component needs.
	addAndMakeVisible(&openButton);
	openButton.setButtonText("Open...");
	openButton.onClick = [this] { openButtonClicked(); };

	addAndMakeVisible(&playButton);
	playButton.setButtonText("Play");
	playButton.onClick = [this] { playButtonClicked(); };
	playButton.setColour(TextButton::buttonColourId, Colours::green);
	playButton.setEnabled(false);

	addAndMakeVisible(&stopButton);
	stopButton.setButtonText("Stop");
	stopButton.onClick = [this] { stopButtonClicked(); };
	stopButton.setColour(TextButton::buttonColourId, Colours::red);
	stopButton.setEnabled(false);

	addAndMakeVisible(&loopingToggle);
	loopingToggle.setButtonText("Loop");
	loopingToggle.onClick = [this] { loopButtonChanged(); };

	addAndMakeVisible(&thumbnailComp);
	addAndMakeVisible(&positionOverlay);

	addAndMakeVisible(&currentPositionLabel);
	currentPositionLabel.setText("Stopped", dontSendNotification);

	setSize(300, 200);

	formatManager.registerBasicFormats();
	transportSource.addChangeListener(this);

	setInterceptsMouseClicks(false, true);

	setAudioChannels(0, 2);

	bufferProcessor = &bufProc;

	startTimer(20);
}

AudioPlayer::~AudioPlayer()
{
	shutdownAudio();
}

//==============================================================================
void AudioPlayer::prepareToPlay(int samplesPerBlockExpected, double sampleRate)
{
	transportSource.prepareToPlay(samplesPerBlockExpected, sampleRate);
}

void AudioPlayer::getNextAudioBlock(const AudioSourceChannelInfo& bufferToFill)
{
	if (readerSource.get() == nullptr)
	{
		bufferToFill.clearActiveBufferRegion();
		return;
	}

	transportSource.getNextAudioBlock(bufferToFill);
	bufferProcessor->getNextAudioBlockTest(bufferToFill);
}

void AudioPlayer::releaseResources()
{
	transportSource.releaseResources();
}

//==============================================================================
void AudioPlayer::paint (Graphics& g)
{
    /* This demo code just fills the component's background and
       draws some placeholder text to get you started.

       You should replace everything in this method with your own
       drawing code..
    */
	
	/*
    g.fillAll (AudioAppComponent::getLookAndFeel().findColour (ResizableWindow::backgroundColourId));   // clear the background

    g.setColour (Colours::white);
    g.drawRect (AudioAppComponent::getLocalBounds(), 1);   // draw an outline around the component

    g.setColour (Colours::white);
    g.setFont (14.0f);
    g.drawText ("AudioPlayer", AudioAppComponent::getLocalBounds(),
                Justification::centred, true);   // draw some placeholder text
	*/
}

void AudioPlayer::resized()
{
    // This method is where you should set the bounds of any child
    // components that your component contains..
	openButton.setBounds(10, 10, AudioAppComponent::getWidth() - 20, 20);
	playButton.setBounds(10, 40, AudioAppComponent::getWidth() - 20, 20);
	stopButton.setBounds(10, 70, AudioAppComponent::getWidth() - 20, 20);
	loopingToggle.setBounds(10, 100, AudioAppComponent::getWidth() - 20, 20);
	currentPositionLabel.setBounds(10, 130, AudioAppComponent::getWidth() - 20, 20);

	Rectangle<int> thumbnailBounds(10, 160, getWidth() - 20, getHeight() - 200);
	thumbnailComp.setBounds(thumbnailBounds);
	positionOverlay.setBounds(thumbnailBounds);
}

//==============================================================================
void AudioPlayer::changeListenerCallback(ChangeBroadcaster* source)
{
	if (source == &transportSource)
	{
		if (transportSource.isPlaying())
			changeState(Playing);
		else if ((state == Stopping) || (state == Playing))
			changeState(Stopped);
		else if (Pausing == state)
			changeState(Paused);
	}
}

void AudioPlayer::timerCallback()
{
	if (transportSource.isPlaying())
	{
		RelativeTime position(transportSource.getCurrentPosition());

		auto minutes = ((int)position.inMinutes()) % 60;
		auto seconds = ((int)position.inSeconds()) % 60;
		auto millis = ((int)position.inMilliseconds()) % 1000;

		auto positionString = String::formatted("%02d:%02d:%03d", minutes, seconds, millis);

		currentPositionLabel.setText(positionString, dontSendNotification);
	}
	else
	{
		currentPositionLabel.setText("Stopped", dontSendNotification);
	}
}

//==============================================================================
void AudioPlayer::updateLoopState(bool shouldLoop)
{
	if (readerSource.get() != nullptr)
		readerSource->setLooping(shouldLoop);
}

void AudioPlayer::closeButtonPressed()
{
	delete this;
}

//==============================================================================
void AudioPlayer::changeState(TransportState newState)
{
	if (state != newState)
	{
		state = newState;

		switch (state)
		{
		case Stopped:
			playButton.setButtonText("Play");
			stopButton.setButtonText("Stop");
			stopButton.setEnabled(false);
			transportSource.setPosition(0.0);
			break;

		case Starting:
			transportSource.start();
			break;

		case Playing:
			playButton.setButtonText("Pause");
			stopButton.setButtonText("Stop");
			stopButton.setEnabled(true);
			break;

		case Pausing:
			transportSource.stop();
			break;

		case Paused:
			playButton.setButtonText("Resume");
			stopButton.setButtonText("Stop");
			break;

		case Stopping:
			transportSource.stop();
			break;
		default:
			jassertfalse;
			break;
		}
	}
}

//==============================================================================
void AudioPlayer::openButtonClicked()
{
	FileChooser chooser("Select a Wave file to play...",
		{},
		"*.wav");

	if (chooser.browseForFileToOpen())
	{
		auto file = chooser.getResult();
		auto* reader = formatManager.createReaderFor(file);

		if (reader != nullptr)
		{
			std::unique_ptr<AudioFormatReaderSource> newSource(new AudioFormatReaderSource(reader, true));
			transportSource.setSource(newSource.get(), 0, nullptr, reader->sampleRate);
			playButton.setEnabled(true);
			thumbnailComp.setFile(file);
			readerSource.reset(newSource.release());
		}
	}
}

void AudioPlayer::playButtonClicked()
{
	updateLoopState(loopingToggle.getToggleState());
	if ((state == Stopped) || (state == Paused))
		changeState(Starting);
	else if (state == Playing)
		changeState(Pausing);
}

void AudioPlayer::stopButtonClicked()
{
	if (state == Paused)
		changeState(Stopped);
	else
		changeState(Stopping);
}

void AudioPlayer::loopButtonChanged()
{
	updateLoopState(loopingToggle.getToggleState());
}