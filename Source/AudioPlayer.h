/*
  ==============================================================================

    AudioPlayer.h
    Created: 21 Apr 2019 4:21:12pm
    Author:  gllm

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "SimpleThumbnail.h"
#include "AudioProcessor.h"

//==============================================================================
class AudioPlayer    :	public AudioAppComponent,
						public ChangeListener,
						public Timer
{
public:
	//==============================================================================
	AudioPlayer(AudioBufferProcessor &bufProc);
	~AudioPlayer();

	//==============================================================================
	void prepareToPlay(int samplesPerBlockExpected, double sampleRate) override;
	void getNextAudioBlock(const AudioSourceChannelInfo& bufferToFill) override;
	void releaseResources() override;

	//==============================================================================
    void paint (Graphics&) override;
	void resized() override;

	//==============================================================================
	void changeListenerCallback(ChangeBroadcaster* source) override;
	void timerCallback() override;

	//==============================================================================
	void updateLoopState(bool shouldLoop);
	void closeButtonPressed();

private:
	//==============================================================================
	enum TransportState
	{
		Stopped,
		Starting,
		Playing,
		Pausing,
		Paused,
		Stopping
	};

	void changeState(TransportState newState);

	//==============================================================================
	void openButtonClicked();
	void playButtonClicked();
	void stopButtonClicked();
	void loopButtonChanged();

	//==========================================================================
	TextButton openButton;
	TextButton playButton;
	TextButton stopButton;
	ToggleButton loopingToggle;
	Label currentPositionLabel;

	AudioFormatManager formatManager;
	std::unique_ptr<AudioFormatReaderSource> readerSource;
	AudioTransportSource transportSource;
	TransportState state;

	AudioThumbnailCache thumbnailCache;
	SimpleThumbnailComponent thumbnailComp;
	SimplePositionOverlay positionOverlay;

	AudioBufferProcessor* bufferProcessor;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (AudioPlayer)
};

//==============================================================================
class AudioPlayerContainer :	public DocumentWindow
{
public:
	AudioPlayerContainer(AudioBufferProcessor &bufProc, const String& name, Colour backgroundColour, int buttonsNeeded)
		: DocumentWindow(name, backgroundColour, buttonsNeeded)
	{
		player = new AudioPlayer(bufProc);
		setContentOwned(player, false);
	}

	~AudioPlayerContainer() {};

	void closeButtonPressed()
	{
		delete this;
	}

private:
	AudioPlayer* player;

	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(AudioPlayerContainer)
};