/*
  ==============================================================================

    AudioProcessor.h
    Created: 23 Apr 2019 1:20:56am
    Author:  gllm

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

//==============================================================================
class AudioBufferProcessor : public AudioAppComponent
{
public:
	//==============================================================================
	AudioBufferProcessor()
	{
		levelSlider.setRange(0.0, 0.5);
		levelSlider.setTextBoxStyle(Slider::TextBoxRight, false, 100, 20);
		levelLabel.setText("Noise Level", dontSendNotification);

		addAndMakeVisible(levelSlider);
		addAndMakeVisible(levelLabel);

		//setSize(600, 100);
		setAudioChannels(0, 2);
	}

	~AudioBufferProcessor()
	{
		shutdownAudio();
	}

	void prepareToPlay(int, double) override {}

	void getNextAudioBlock(const AudioSourceChannelInfo& bufferToFill) override
	{
	/*
		auto* device = deviceManager.getCurrentAudioDevice();
		auto activeInputChannels = device->getActiveInputChannels();
		auto activeOutputChannels = device->getActiveOutputChannels();
		auto maxInputChannels = activeInputChannels.getHighestBit() + 1;
		auto maxOutputChannels = activeOutputChannels.getHighestBit() + 1;

		auto level = (float)levelSlider.getValue();

		for (auto channel = 0; channel < maxOutputChannels; ++channel)
		{
			if ((!activeOutputChannels[channel]) || maxInputChannels == 0)
			{
				bufferToFill.buffer->clear(channel, bufferToFill.startSample, bufferToFill.numSamples);
			}
			else
			{
				auto actualInputChannel = channel % maxInputChannels; // [1]

				if (!activeInputChannels[channel]) // [2]
				{
					bufferToFill.buffer->clear(channel, bufferToFill.startSample, bufferToFill.numSamples);
				}
				else // [3]
				{
					auto* inBuffer = bufferToFill.buffer->getReadPointer(actualInputChannel,
						bufferToFill.startSample);
					auto* outBuffer = bufferToFill.buffer->getWritePointer(channel, bufferToFill.startSample);

					for (auto sample = 0; sample < bufferToFill.numSamples; ++sample)
						outBuffer[sample] = inBuffer[sample] * level;
				}
			}
		}
		*/
	}

	void getNextAudioBlockTest(const AudioSourceChannelInfo& bufferToFill)
	{
		auto level = (float)levelSlider.getValue();
		auto levelScale = level * 2.0f;

		for (auto channel = 0; channel < bufferToFill.buffer->getNumChannels(); ++channel)
		{
			auto* buffer = bufferToFill.buffer->getWritePointer(channel, bufferToFill.startSample);

			for (auto sample = 0; sample < bufferToFill.numSamples; ++sample)
				buffer[sample] = buffer[sample] * levelScale - level;
		}
	}

	void releaseResources() override {}

	void resized() override
	{
		levelLabel.setBounds(10, 10, 90, 20);
		levelSlider.setBounds(100, 10, getWidth() - 110, 20);
	}

private:
	Slider levelSlider;
	Label levelLabel;

	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(AudioBufferProcessor)
};

//==============================================================================
class AudioProcessorContainer : public DocumentWindow
{
public:
	//==============================================================================
	AudioProcessorContainer(const String& name, Colour backgroundColour, int buttonsNeeded)
		: DocumentWindow(name, backgroundColour, buttonsNeeded)
	{
		setContentOwned(&processor, false);
	}

	~AudioProcessorContainer() {};

	void closeButtonPressed()
	{
		delete this;
	}

	AudioBufferProcessor processor;
private:

	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(AudioProcessorContainer)
};