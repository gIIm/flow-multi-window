/*
  ==============================================================================

    ColourUtils.h
    Created: 20 Apr 2019 9:47:40pm
    Author:  gllm

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

#ifndef COLOUR_UTILS_INCLUDED
#define COLOUR_UTILS_INCLUDED 1
#endif

inline Colour getRandomColour(float brightness) noexcept
{
	return Colour::fromHSV(Random::getSystemRandom().nextFloat(), 0.5f, brightness, 1.0f);
}

inline Colour getRandomDarkColour() noexcept { return getRandomColour(0.3f); }

inline Colour getUIColourIfAvailable(LookAndFeel_V4::ColourScheme::UIColour uiColour, Colour fallback = Colour(0xff4d4d4d)) noexcept
{
	if (auto* v4 = dynamic_cast<LookAndFeel_V4*> (&LookAndFeel::getDefaultLookAndFeel()))
		return v4->getCurrentColourScheme().getUIColour(uiColour);

	return fallback;
}