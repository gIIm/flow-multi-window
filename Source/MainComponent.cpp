/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"

//==============================================================================
MainComponent::MainComponent()
{
	setSize(200, 200);

	setOpaque(true);

	addAndMakeVisible(showWindowsButton);
	showWindowsButton.onClick = [this] { showAllWindows(); };

	addAndMakeVisible(closeWindowsButton);
	closeWindowsButton.onClick = [this] { closeAllWindows(); };

    // specify the number of input and output channels that we want to open
    setAudioChannels (0, 2);
}

MainComponent::~MainComponent()
{
	closeAllWindows();

    // This shuts down the audio device and clears the audio source.
    shutdownAudio();
}

//==============================================================================
void MainComponent::prepareToPlay (int samplesPerBlockExpected, double sampleRate)
{
    // This function will be called when the audio device is started, or when
    // its settings (i.e. sample rate, block size, etc) are changed.

    // You can use this function to initialise any resources you might need,
    // but be careful - it will be called on the audio thread, not the GUI thread.

    // For more details, see the help for AudioProcessor::prepareToPlay()
}

void MainComponent::getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill)
{
    // Your audio-processing code goes here!

    // For more details, see the help for AudioProcessor::getNextAudioBlock()

    // Right now we are not producing any data, in which case we need to clear the buffer
    // (to prevent the output of random noise)
    bufferToFill.clearActiveBufferRegion();
}

void MainComponent::releaseResources()
{
    // This will be called when the audio device stops, or when it is being
    // restarted due to a setting change.

    // For more details, see the help for AudioProcessor::releaseResources()
}

//==============================================================================
void MainComponent::paint (Graphics& g)
{
    // You can add your drawing code here!
	g.fillAll(Colour(42,42,42));
}

void MainComponent::resized()
{
    // This is called when the MainContentComponent is resized.
    // If you add any child components, this is where you should
    // update their positions.
	Rectangle<int> buttonSize(0, 0, 108, 28);

	Rectangle<int> area((getWidth() / 2) - (buttonSize.getWidth() / 2),
		(getHeight() / 2) - buttonSize.getHeight(),
		buttonSize.getWidth(), buttonSize.getHeight());

	showWindowsButton.setBounds(area.reduced(2));
	closeWindowsButton.setBounds(area.translated(0, buttonSize.getHeight()).reduced(2));
}

//==============================================================================
void MainComponent::showAllWindows()
{
	closeAllWindows();

	showAudioManagerWindow(10, 10);

	showAudioPlayerWindow(10,360);
	showAudioPlayerWindow(410,360);
}

void MainComponent::closeAllWindows()
{
	for (auto& window : windows)
		window.deleteAndZero();

	windows.clear();
}

void MainComponent::showAudioManagerWindow(int x, int y)
{
	auto* manager = new AudioManagerContainer("AudioManager", getRandomDarkColour(), DocumentWindow::allButtons);
	windows.add(manager);

	Rectangle<int> area(0, 0, 400, 350);

	RectanglePlacement placement(RectanglePlacement::xLeft
		| RectanglePlacement::yTop
		| RectanglePlacement::doNotResize);

	auto result = placement.appliedTo(area, Desktop::getInstance().getDisplays()
		.getMainDisplay().userArea.reduced(0));
	manager->setBounds(result);

	manager->setResizable(true, false);
	manager->setUsingNativeTitleBar(false);
	manager->setTopLeftPosition(x, y);
	manager->setVisible(true);
}

void MainComponent::showAudioPlayerWindow(int x, int y)
{
	AudioProcessorContainer* processor = new AudioProcessorContainer("AudioProcessor", Colour(12, 12, 12), DocumentWindow::allButtons);

	//showAudioProcessorWindow(*processor, x, y + 400);

	auto* player = new AudioPlayerContainer(processor->processor, "AudioPlayer", getRandomDarkColour(), DocumentWindow::allButtons);
	windows.add(player);

	Rectangle<int> area(0, 0, 400, 400);

	RectanglePlacement placement(RectanglePlacement::xLeft
		| RectanglePlacement::yTop
		| RectanglePlacement::doNotResize);

	auto result = placement.appliedTo(area, Desktop::getInstance().getDisplays()
		.getMainDisplay().userArea.reduced(0));
	player->setBounds(result);

	player->setResizable(true, false);
	player->setUsingNativeTitleBar(false);
	player->setTopLeftPosition(x, y);
	player->setVisible(true);
}

void MainComponent::showAudioProcessorWindow(AudioProcessorContainer &proc, int x, int y)
{
	//AudioProcessorContainer* processor = new AudioProcessorContainer("AudioProcessor", Colour(12, 12, 12), DocumentWindow::allButtons);
	AudioProcessorContainer* processor = &proc;
	windows.add(processor);

	Rectangle<int> area(0, 0, 300, 200);

	RectanglePlacement placement(RectanglePlacement::xLeft
		| RectanglePlacement::yTop
		| RectanglePlacement::doNotResize);

	auto result = placement.appliedTo(area, Desktop::getInstance().getDisplays()
		.getMainDisplay().userArea.reduced(0));
	processor->setBounds(result);

	processor->setResizable(true, false);
	processor->setUsingNativeTitleBar(false);
	processor->setTopLeftPosition(x, y);
	processor->setVisible(true);
}