/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "BasicWindow.h"
#include "ColourUtils.h"
#include "AudioManager.h"
#include "AudioPlayer.h"

//==============================================================================
/*
This component lives inside our window, and this is where you should put all
your controls and content.
*/
class MainComponent : public AudioAppComponent
{
public:
	//==============================================================================
	MainComponent();
	~MainComponent();

	//==============================================================================
	void prepareToPlay(int samplesPerBlockExpected, double sampleRate) override;
	void getNextAudioBlock(const AudioSourceChannelInfo& bufferToFill) override;
	void releaseResources() override;

	//==============================================================================
	void paint(Graphics& g) override;
	void resized() override;

private:
	//==============================================================================
	// Your private member variables go here...
	// Because in this demo the windows delete themselves, we'll use the
	// Component::SafePointer class to point to them, which automatically becomes
	// null when the component that it points to is deleted.
	Array<Component::SafePointer<Component>> windows;

	TextButton showWindowsButton{ "Show Windows" },
		closeWindowsButton{ "Close Windows" };

	//==============================================================================
	void showAllWindows();
	void closeAllWindows();
	void showAudioManagerWindow(int x, int y);
	void showAudioPlayerWindow(int x, int y);
	void showAudioProcessorWindow(AudioProcessorContainer &proc, int x, int y);

	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(MainComponent)
};