/*
  ==============================================================================

    SimpleThumbnail.cpp
    Created: 22 Apr 2019 6:31:53pm
    Author:  gllm

  ==============================================================================
*/

#include "SimpleThumbnail.h"

//==============================================================================
SimpleThumbnailComponent::SimpleThumbnailComponent(int sourceSamplesPerThumbnailSample,
	AudioFormatManager& formatManager,
	AudioThumbnailCache& cache)
	: thumbnail(sourceSamplesPerThumbnailSample, formatManager, cache)
{
	thumbnail.addChangeListener(this);
}

void SimpleThumbnailComponent::setFile(const File& file)
{
	thumbnail.setSource(new FileInputSource(file));
}

void SimpleThumbnailComponent::paint(Graphics& g)
{
	if (thumbnail.getNumChannels() == 0)
		paintIfNoFileLoaded(g);
	else
		paintIfFileLoaded(g);
}

void SimpleThumbnailComponent::paintIfNoFileLoaded(Graphics& g)
{
	g.fillAll(Colours::white);
	g.setColour(Colours::darkgrey);
	g.drawFittedText("No File Loaded", getLocalBounds(), Justification::centred, 1.0f);
}

void SimpleThumbnailComponent::paintIfFileLoaded(Graphics& g)
{
	g.fillAll(Colours::white);

	g.setColour(Colours::red);
	thumbnail.drawChannels(g, getLocalBounds(), 0.0, thumbnail.getTotalLength(), 1.0f);
}

void SimpleThumbnailComponent::changeListenerCallback(ChangeBroadcaster* source)
{
	if (source == &thumbnail)
		thumbnailChanged();
}


void SimpleThumbnailComponent::thumbnailChanged()
	{
		repaint();
	}

//==============================================================================

SimplePositionOverlay::SimplePositionOverlay(AudioTransportSource& transportSourceToUse)
	: transportSource(transportSourceToUse)
{
	startTimer(40);
}

void SimplePositionOverlay::paint(Graphics& g)
{
	auto duration = transportSource.getLengthInSeconds();

	if (duration > 0.0)
	{
		auto audioPosition = transportSource.getCurrentPosition();
		auto drawPosition = (audioPosition / duration) * getWidth();

		g.setColour(Colours::green);
		g.drawLine(drawPosition, 0.0f, drawPosition, (float)getHeight(), 2.0f);
	}
}

void SimplePositionOverlay::mouseDown(const MouseEvent& event)
{
	auto duration = transportSource.getLengthInSeconds();

	if (duration > 0.0)
	{
		auto clickPosition = event.position.x;
		auto audioPosition = (clickPosition / getWidth()) * duration;

		transportSource.setPosition(audioPosition);
	}
}

void SimplePositionOverlay::timerCallback()
{
	repaint();
}

