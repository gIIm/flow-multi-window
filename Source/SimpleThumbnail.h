/*
  ==============================================================================

    SimpleThumbnail.h
    Created: 22 Apr 2019 6:31:53pm
    Author:  gllm

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

//==============================================================================
class SimpleThumbnailComponent : public Component,
	private ChangeListener
{
public:
	SimpleThumbnailComponent(int sourceSamplesPerThumbnailSample,
		AudioFormatManager& formatManager,
		AudioThumbnailCache& cache);

	void setFile(const File& file);

	void paint(Graphics& g) override;

	void paintIfNoFileLoaded(Graphics& g);

	void paintIfFileLoaded(Graphics& g);

	void changeListenerCallback(ChangeBroadcaster* source) override;

private:
	void thumbnailChanged();

	AudioThumbnail thumbnail;

	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(SimpleThumbnailComponent)
};

//==============================================================================
class SimplePositionOverlay : public Component,
	private Timer
{
public:
	SimplePositionOverlay(AudioTransportSource& transportSourceToUse);

	void paint(Graphics& g) override;

	void mouseDown(const MouseEvent& event) override;

private:
	void timerCallback() override;

	AudioTransportSource& transportSource;

	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(SimplePositionOverlay)
};